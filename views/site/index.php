<?php

use app\models\Source;

/* @var yii\base\View $this */
/* @var string $selectedDataSource */
/* @var integer $selectedViewType */
/* @var Source[] $sources*/

$viewTypes = array(
    0 => 'Table',
    1 => 'Lines',
    2 => 'Bars',
);

// Load JSON data
$columns = $rows = null;
if(!empty($selectedDataSource)) {
    $data = Source::loadSource($selectedDataSource);
    $columns = $data['Columns'] ?: null;
    $rows = $data['Rows'] ?: null;
}
?>
<div class="row">
    <!-- Configuration block START -->
    <div class="col-sm-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Configuration</h3>
            </div>
            <div class="panel-body">
                <form action="?" method="post">
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

                    <div class="form-group">
                        <label for="data-source">Data source:</label>
                        <select class="form-control" id="data-source" name="data-source">
                            <?php foreach ($sources as $source) { ?>
                                <option<?= $selectedDataSource == $source->filename ? ' selected="selected"' : '' ?>><?= $source->filename ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="view-type">View type:</label>
                        <select class="form-control" id="view-type" name="view-type">
                            <?php foreach ($viewTypes as $viewIdx => $viewType) { ?>
                                <option value="<?= $viewIdx ?>"<?= $selectedViewType == $viewIdx ? ' selected="selected"' : '' ?>><?= $viewType ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Refresh&nbsp;&raquo;</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Configuration block END -->
    <!-- Presentation block START -->
    <div class="col-sm-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Presentation</h3>
            </div>
            <div class="panel-body">
                <?php if(is_null($rows)) { ?>
                    No data loaded.
                <?php } else { ?>
                    <?php if($selectedViewType == 0) { // Table view
                        echo $this->render('_table', [
                            'columns' => $columns,
                            'rows' => $rows,
                        ]);
                    } else if($selectedViewType == 1) { // Lines
                        echo $this->render('_chart', [
                            'columns' => $columns,
                            'rows' => $rows,
                            'lines' => true,
                        ]);
                    } else { // Bars
                        echo $this->render('_chart', [
                            'columns' => $columns,
                            'rows' => $rows,
                            'lines' => false,
                        ]);
                    }
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Presentation block END -->
</div>
