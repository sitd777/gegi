<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="error-page">
    <h2 class="headline text-yellow"><?php echo property_exists($exception, 'statusCode') ? $exception->statusCode : '&nbsp;' ?></h2>

    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Oops! <?php echo $message ?></h3>
    </div>

</div>
