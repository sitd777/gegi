<?php
$bgColors = array(
'rgba(255, 99, 132, 0.5)', // red
'rgba(255, 159, 64, 0.5)', // orange
'rgba(255, 205, 86, 0.5)', // yellow
'rgba(75, 192, 192, 0.5)', // green
'rgba(54, 162, 235, 0.5)', //blue
'rgba(153, 102, 255, 0.5)', // purple
'rgba(201, 203, 207, 0.5)', // grey
);
$borderColors = array(
'rgb(255, 99, 132)', // red
'rgb(255, 159, 64)', // orange
'rgb(255, 205, 86)', // yellow
'rgb(75, 192, 192)', // green
'rgb(54, 162, 235)', //blue
'rgb(153, 102, 255)', // purple
'rgb(201, 203, 207)', // grey
);
$dataSets = array();
$i = 0;
foreach((array)$rows as $rowIndex => $rowData) {
$dataSet = array();
$dataSet['label'] = $rowIndex;
$dataSet['backgroundColor'] = isset($bgColors[$i]) ? $bgColors[$i] : $bgColors[0];
$dataSet['borderColor'] = isset($borderColors[$i]) ? $borderColors[$i] : $borderColors[0];
$dataSet['borderWidth'] = 1;
$dataSet['data'] = $rowData;
$dataSets[] = (object)$dataSet;
$i++;
}
?>
<canvas id="canvas"></canvas>
<script>
    'use strict';
    <?php if($lines) {
    foreach ($dataSets as $datasetIndex => $dataSet) $dataSets[$datasetIndex]->fill = false;
    ?>
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?= json_encode($columns) ?>,
                datasets: <?= json_encode($dataSets) ?>
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: ''
                }
            }
        });

    };
    <?php } else { ?>
    window.onload = function() {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: <?= json_encode($columns) ?>,
                datasets: <?= json_encode($dataSets) ?>
            },
            options: {
                responsive: true,
                title:{
                    display:true,
                    text:''
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        });

    };
    <?php } ?>
</script>