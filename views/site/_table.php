<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <?php foreach($columns as $columnIndex => $columnName) { ?>
                <th><?= $columnName ?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach((array)$rows as $rowIndex => $rowData) { ?>
            <tr>
                <?php foreach($columns as $columnIndex => $columnName) { ?>
                    <td><?= isset($rowData[$columnIndex]) ? $rowData[$columnIndex] : 0 ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>