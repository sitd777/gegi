<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::$app->name . ($this->title ? ': ' . $this->title : '')) ?></title>
    <?php $this->head() ?>
    <!-- ChartJS -->
    <script src="//www.chartjs.org/dist/2.7.0/Chart.bundle.js"></script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-default navbar-fixed-top',
        ],
    ]);

    $menuItems = [
        ['label' => Yii::t('app', 'Data presentation'), 'url' => ['/site/index']],
        ['label' => Yii::t('app', 'Data sources'), 'url' => ['/sources/index']],
    ];

    echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav'],
        'items'   => $menuItems,
    ]);

    NavBar::end();
    ?>

    <div class="container main">
        <section class="messages">
            <?php echo \app\widgets\Flashes::widget() ?>
        </section>

        <section class="content">
            <?= $content ?>
        </section>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
