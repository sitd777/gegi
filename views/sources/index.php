<?php

use app\models\Source;
use yii\helpers\Url;

?>

<div class="row">
    <!-- Data source upload form START -->
    <div class="col-sm-6">
        <div class="well">
            <form enctype="multipart/form-data" method="post" action="<?= Url::to(['sources/upload']) ?>">
                <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                <div class="form-group">
                    <label for="new-data-source">Upload new data source</label>
                    <input type="file" name="new-data-source" class="form-control-file" id="new-data-source">
                </div>
                <div class="form-group">
                    <button type="submit" name="upload" class="btn btn-primary">Upload&nbsp;&raquo;</button>
                </div>
            </form>
        </div>
    </div>
    <!-- Data source upload form END -->
    <!-- Data source list START -->
    <div class="col-sm-6">
        <ul class="list-group">
            <?php foreach (Source::findAll() as $fileModel) { ?>
            <li class="list-group-item"><?= $fileModel->filename ?>
                <div class="pull-right"><a href="<?= Url::to(['sources/delete', 'file' => $fileModel->filename]) ?>" onclick="return confirm('Are You sure?')" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove-sign"></i></a></div>
            </li>
        <?php } ?>
        </ul>
    </div>
    <!-- Data source list END -->
</div>