<?php

namespace app\models;

use yii\base\Model;
use Yii;

/**
 * Class Source
 * @package app\models
 *
 * @property string $filename
 */
class Source extends Model {

    /**
     * Holds datasources directory path
     */
    const PATH_SOURCES = '@runtime/sources/';

    /**
     * Holds file file nam
     * @var string
     */
    public $filename = '';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'filename' => 'File name',
        ];
    }

    /**
     * Get list of sources
     *
     * @param boolean $useCache
     * @return array
     */
    public static function findAll($useCache = true)
    {
        static $sources = array();
        if(sizeof($sources) == 0 || !$useCache) {
            $dirPath = Yii::getAlias(static::PATH_SOURCES);
            $sources = [];
            if(file_exists($dirPath)) {
                $dh = opendir($dirPath);
                while ($file = readdir($dh)) {
                    if ($file == '.' || $file == '..') continue;
                    $info = pathinfo($file);
                    if ($info['extension'] != 'json') continue;
                    $sources[] = new Source(['filename' => $file]);
                }
                closedir($dh);
            }
        }
        return $sources;
    }

    /**
     * Uploads new data source
     *
     * @param array $fileData
     * @return Source
     */
    public static function uploadSource($fileData) {
        $dirPath = Yii::getAlias(static::PATH_SOURCES);
        if(!file_exists($dirPath)) mkdir($dirPath);

        $newDataSourceName = date('Y-m-d_His') . '.json';
        if(copy($fileData['tmp_name'], $dirPath . $newDataSourceName)) {
            return new Source(['filename' => $newDataSourceName]);
        }
        return null;
    }

    /**
     * Deletes existing data source
     *
     * @param string $fileName
     * @return boolean
     */
    public static function deleteSource($fileName) {
        $dirPath = Yii::getAlias(static::PATH_SOURCES);
        if(file_exists($dirPath . $fileName)) {
            unlink($dirPath . $fileName);
            return true;
        }
        return false;
    }

    /**
     * Checks existing data file
     *
     * @param string $fileName
     * @return boolean
     */
    public static function checkSource($fileName) {
        $dirPath = Yii::getAlias(static::PATH_SOURCES);
        return file_exists($dirPath . $fileName);
    }

    /**
     * Load existing data file
     *
     * @param string $fileName
     * @return boolean
     */
    public static function loadSource($fileName) {
        $dirPath = Yii::getAlias(static::PATH_SOURCES);
        if(!file_exists($dirPath . $fileName)) return [];

        $fileContents = file_get_contents($dirPath . $fileName);
        return (array)json_decode($fileContents);
    }
}