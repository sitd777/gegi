<?php

namespace app\controllers;

use app\models\Source;
use app\widgets\Flashes;
use Yii;
use yii\base\Request;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Shows presentation of data sources
     *
     * @return string
     */
    public function actionIndex()
    {
        /* @var \yii\web\Session $session */
        $session = Yii::$app->session;

        /* @var Request $request */
        $request = Yii::$app->request;


        /* @var Source $sources */
        $sources = Source::findAll();

        // Load saved configuration
        $sessData = $session->get('presentation', []);
        $firstSource = sizeof($sources) ? $sources[0]->filename : '';
        $selectedDataSource = !empty($sessData['data-source']) ? $sessData['data-source'] : $firstSource;
        $selectedViewType = isset($sessData['view-type']) ? $sessData['view-type'] : 0;

        // Get configuration selected by user
        $postDataSource = $request->post('data-source');
        if(!empty($postDataSource)) $selectedDataSource = $postDataSource;
        $postViewType = $request->post('view-type', -1);
        if($postViewType > -1) $selectedViewType = $postViewType;

        // Check configuration
        if(!Source::checkSource($selectedDataSource)) $selectedDataSource = $firstSource;
        if($selectedViewType < 0 || $selectedViewType > 2) $selectedViewType = 0;

        // Save configuration
        $sessData['data-source'] = $selectedDataSource;
        $sessData['view-type'] = $selectedViewType;
        $session->set('presentation', $sessData);

        return $this->render('index', [
            'selectedDataSource' => $selectedDataSource,
            'selectedViewType' => $selectedViewType,
            'sources' => $sources,
        ]);
    }
}
