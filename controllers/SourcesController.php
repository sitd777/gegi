<?php

namespace app\controllers;

use app\models\Api;
use app\models\Client;
use app\models\Source;
use app\models\Venicle;
use app\widgets\Flashes;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SourcesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Shows list of data sources
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Uploads new data source
     */
    public function actionUpload()
    {
        // Process data source upload
        $upload = isset($_POST['upload']);
        if($upload && isset($_FILES['new-data-source'])) {
            $fileData = $_FILES['new-data-source'];
            if($fileData['error']) {
                Flashes::setError('Data source upload error! Error code: ' . $fileData['error']);
            } else if(!file_exists($fileData['tmp_name'])) {
                Flashes::setError('Data source upload error! File does not exist!');
            } else {
                /** @var Source $result */
                $result = Source::uploadSource($fileData);
                if(!$result) {
                    Flashes::setError('Data source upload error!');
                } else {
                    Flashes::setSuccess('New data source saved as `' . $result->filename . '`.');
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Deletes data source from list
     */
    public function actionDelete()
    {
        $file = Yii::$app->request->get('file', '');
        if($file) {
            $result = Source::deleteSource($file);
            if(!$result) {
                Flashes::setError('Data source deletion error!');
            } else {
                Flashes::setSuccess('Data source was successfully deleted! File: ' . $file);
            }
        }
        return $this->redirect(['index']);
    }
}
